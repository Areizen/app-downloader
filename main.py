#!/usr/bin/python3
# coding: utf8

import requests
import sys

from bs4 import BeautifulSoup

base_url = "https://apkpure.com"
search_url = base_url + "/search?q="

if (len(sys.argv) < 2 ):
	print(f"Usage : {sys.argv[0]} <app-name>")
	sys.exit()


r = requests.get(search_url + sys.argv[1])
soup = BeautifulSoup(r.text, 'html.parser')

print("[+] Choose the desired application :")

apps_list = soup.find_all('p',{'class':'search-title'})

for i in range(len(apps_list)):
	print( "\t" + str(i + 1) + ' - ' + apps_list[i].a.text)

app_number = int(input('> ')) - 1 

relative_link = apps_list[app_number].a['href']
r = requests.get( base_url + relative_link )

soup = BeautifulSoup(r.text, 'html.parser')
download_link = soup.find('a',{'class' : 'da'})['href']
r = requests.get(base_url + download_link)
soup = BeautifulSoup(r.text, 'html.parser')
download = soup.find('a', {'id' : 'download_link'})['href']

with open(str(apps_list[app_number].a.text)+'.apk','wb') as f:
        print("Downloading %s" % str(apps_list[app_number].a.text)+'.apk')
        response = requests.get(download, stream=True)
        total_length = response.headers.get('content-length')

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sys.stdout.write("\r[%s%s] %s%%" % ('=' * done, ' ' * (50-done),str(100 * float(dl) / float(total_length)) )    )
                sys.stdout.flush()
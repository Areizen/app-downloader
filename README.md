# App-Downloader

Quick & Dirty tool to download applications from ApkPure.

## Usage 

This tool use python3

```
pip install -r requirements.txt --user
python3 main.py <app-name>
```
